package com.fusa.server.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fusa.server.vo.HelloVO;

@Repository
public class HelloDAO {
      
      @Autowired
      private SqlSession SqlSession;
      
      public List<HelloVO> getTestValue(HelloVO helloVO){
            return SqlSession.selectList("com.fusa.server.getTestValue", helloVO);
      }
      
      public List<HelloVO> getValue(){
    	  return SqlSession.selectList("com.fusa.server.getValue");
      }
      
}
