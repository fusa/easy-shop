package com.fusa.server.vo;

public class HelloVO {

	private String codeId;
	private String codeDetailId;
	private String codeDetailName;
	private String codeDetailDescription;

	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCodeDetailId() {
		return codeDetailId;
	}

	public void setCodeDetailId(String codeDetailId) {
		this.codeDetailId = codeDetailId;
	}

	public String getCodeDetailName() {
		return codeDetailName;
	}

	public void setCodeDetailName(String codeDetailName) {
		this.codeDetailName = codeDetailName;
	}

	public String getCodeDetailDescription() {
		return codeDetailDescription;
	}

	public void setCodeDetailDescription(String codeDetailDescription) {
		this.codeDetailDescription = codeDetailDescription;
	}

}
