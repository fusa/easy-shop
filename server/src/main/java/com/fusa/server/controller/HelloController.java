package com.fusa.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fusa.server.service.HelloService;
import com.fusa.server.vo.HelloVO;

@Controller
public class HelloController {
	
	@Autowired
	private HelloService HelloService;
	
	@RequestMapping("hello")
	public String hello(Model model) throws Exception {
		
		List<HelloVO> list = HelloService.getValue();
		System.out.println(list);
		if(list.size() >0) {
			model.addAttribute("list", list);
		} else {
			model.addAttribute("", "");
		}
			
		
		return "hello";
	}
}