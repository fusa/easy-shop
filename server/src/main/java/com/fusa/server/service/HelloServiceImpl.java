package com.fusa.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fusa.server.dao.HelloDAO;
import com.fusa.server.vo.HelloVO;

@Service("HelloService")
@Transactional(rollbackFor = {Exception.class})
public class HelloServiceImpl implements HelloService {
      
      @Autowired
      HelloDAO HelloDAO;
      
      @Override
      public List<HelloVO> getTestValue(HelloVO helloVO){
            return HelloDAO.getTestValue(helloVO);
      }
      
      @Override
      public List<HelloVO> getValue(){
    	  System.out.println("rrrrrrrrrrrrrrrrrrrrrrr");
    	  return HelloDAO.getValue();
      }
      
      
}