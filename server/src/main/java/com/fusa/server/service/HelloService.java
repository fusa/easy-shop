package com.fusa.server.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.fusa.server.vo.HelloVO;

@Service
public interface HelloService {

	public List<HelloVO> getTestValue(HelloVO helloVO);
	
	public List<HelloVO> getValue();
}
