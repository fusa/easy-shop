# 프로젝트 개요 

개인 개발 용도로 와이프와 함께 만드는 쇼핑몰 플랫폼 구축 프로젝트

---

## 개발 스펙
```
Client, Server, DB로 세분화 
```
### Client
```
Vue, Vuex, Vue-Router, Vuetify
```
### Server
```
Java SpringFramework
```
### DB
```
ORM : Mybatis
DB : Maria
```